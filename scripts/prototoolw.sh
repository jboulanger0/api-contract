if [[ "$(docker images -q myimage:mytag 2> /dev/null)" != "" ]]; then
  docker build -t prototool -f Dockerfile .
fi

exec docker run -v $(pwd):/home --rm --ulimit memlock=-1 -it prototool $@