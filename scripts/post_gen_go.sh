mod_prefix=$(git config --get remote.origin.url | awk '{gsub(/git@|\.git/,"")}1' | awk '{gsub(/:/,"/")}1')

for p in $(find generated/go/contracts/ -type f | xargs dirname | uniq | tr -s /)
do  
    gomod_path="${p}/go.mod"
    if [[ ! -f $gomod_path ]]; then
        module_name="${mod_prefix}/${p}"
        $(cd $p && go mod init ${module_name})
    fi
    $(cd $p && go mod tidy)
done