# Swift grpc plugin
FROM swift:5.1 as swift-grpc-plugin

ENV GRPC_SWIFT_VERSION "0.11.0"

RUN git clone --depth 1 --branch ${GRPC_SWIFT_VERSION} https://github.com/grpc/grpc-swift.git /grpc-swift
RUN cd /grpc-swift && make plugin 
RUN cp /grpc-swift/protoc-gen-swiftgrpc /usr/local/bin/protoc-gen-grpc-swift
RUN cp /grpc-swift/protoc-gen-swift /usr/local/bin/protoc-gen-swift 


# Kotlin grpc plugin
FROM openjdk:8 as kotlin-grpc-plugin

ENV GRPC_KOTLIN_VERSION "0.1.4"

RUN git clone --depth 1 --branch v${GRPC_KOTLIN_VERSION} https://github.com/grpc/grpc-kotlin.git /grpc-kotlin
RUN cd /grpc-kotlin/compiler && ../gradlew build 
RUN cd /grpc-kotlin/compiler && ../gradlew prependShellStub 
RUN cp /grpc-kotlin/compiler/build/artifacts/protoc-gen-grpc-kotlin /usr/local/bin/protoc-gen-grpckt


# Kotlin plugin
FROM openjdk:8 as kotlin-plugin

ENV STREEM_PBANDK_VERSION "0.8.1"

RUN git clone --depth 1 --branch v${STREEM_PBANDK_VERSION} https://github.com/streem/pbandk.git /kotlin
RUN cd /kotlin && ./gradlew :protoc-gen-kotlin:jvm:assembleDist
RUN unzip /kotlin/protoc-gen-kotlin/jvm/build/distributions/protoc-gen-kotlin-${STREEM_PBANDK_VERSION}.zip
RUN cp /protoc-gen-kotlin-${STREEM_PBANDK_VERSION}/bin/protoc-gen-kotlin /usr/local/bin/protoc-gen-kotlin
RUN mkdir -p /protoc-gen-kotlin/ && cp -r /protoc-gen-kotlin-${STREEM_PBANDK_VERSION}/lib/ /protoc-gen-kotlin/


# JS plugin
FROM node:14 as js-plugin

ENV GRPC_WEB_VERSION "1.2.0"

RUN npm i protoc-gen-grpc-web@${GRPC_WEB_VERSION}
RUN cp /node_modules/protoc-gen-grpc-web/bin/protoc-gen-grpc-web /usr/local/bin/protoc-gen-grpc-web


# Go plugin
FROM golang:1.14 as go-plugin

RUN go get github.com/golang/protobuf/protoc-gen-go
RUN go get github.com/grpc-ecosystem/grpc-gateway/protoc-gen-grpc-gateway
RUN go get github.com/grpc/grpc-go/cmd/protoc-gen-go-grpc
RUN go build -o /usr/local/bin/protoc-gen-go github.com/golang/protobuf/protoc-gen-go
RUN go build -o /usr/local/bin/protoc-gen-go-grpc github.com/grpc/grpc-go/cmd/protoc-gen-go-grpc
RUN go build -o /usr/local/bin/protoc-gen-grpc-gateway github.com/grpc-ecosystem/grpc-gateway/protoc-gen-grpc-gateway


# Swagger plugin
FROM golang:1.14 as swagger-plugin

RUN go get github.com/grpc-ecosystem/grpc-gateway/protoc-gen-swagger
RUN go build -o /usr/local/bin/protoc-gen-swagger github.com/grpc-ecosystem/grpc-gateway/protoc-gen-swagger


# Build prototool
FROM uber/prototool as builder

RUN apk add --no-cache make openjdk8-jre && \
    rm -rf /var/cache/apk/*

COPY --from=swift-grpc-plugin /usr/local/bin/protoc-gen-swift /usr/local/bin/
COPY --from=swift-grpc-plugin /usr/local/bin/protoc-gen-grpc-swift /usr/local/bin/
# from ldd protoc-gen-swift:
COPY --from=swift-grpc-plugin /lib/x86_64-linux-gnu/libc.so.6 /lib/x86_64-linux-gnu/libc.so.6
COPY --from=swift-grpc-plugin /lib/x86_64-linux-gnu/libdl.so.2 /lib/x86_64-linux-gnu/libdl.so.2
COPY --from=swift-grpc-plugin /lib/x86_64-linux-gnu/libgcc_s.so.1 /lib/x86_64-linux-gnu/libgcc_s.so.1
COPY --from=swift-grpc-plugin /lib/x86_64-linux-gnu/libm.so.6 /lib/x86_64-linux-gnu/libm.so.6
COPY --from=swift-grpc-plugin /lib/x86_64-linux-gnu/libpthread.so.0 /lib/x86_64-linux-gnu/libpthread.so.0
COPY --from=swift-grpc-plugin /lib/x86_64-linux-gnu/librt.so.1 /lib/x86_64-linux-gnu/librt.so.1
COPY --from=swift-grpc-plugin /lib/x86_64-linux-gnu/libutil.so.1 /lib/x86_64-linux-gnu/libutil.so.1
COPY --from=swift-grpc-plugin /lib/x86_64-linux-gnu/libz.so.1 /lib/x86_64-linux-gnu/libz.so.1
COPY --from=swift-grpc-plugin /usr/lib/swift/linux/libBlocksRuntime.so  /usr/lib/swift/linux/libBlocksRuntime.so
COPY --from=swift-grpc-plugin /usr/lib/swift/linux/libFoundation.so /usr/lib/swift/linux/libFoundation.so
COPY --from=swift-grpc-plugin /usr/lib/swift/linux/libdispatch.so /usr/lib/swift/linux/libdispatch.so
COPY --from=swift-grpc-plugin /usr/lib/swift/linux/libicudataswift.so.61 /usr/lib/swift/linux/libicudataswift.so.61
COPY --from=swift-grpc-plugin /usr/lib/swift/linux/libicui18nswift.so.61 /usr/lib/swift/linux/libicui18nswift.so.61
COPY --from=swift-grpc-plugin /usr/lib/swift/linux/libicuucswift.so.61  /usr/lib/swift/linux/libicuucswift.so.61
COPY --from=swift-grpc-plugin /usr/lib/swift/linux/libswiftCore.so /usr/lib/swift/linux/libswiftCore.so
COPY --from=swift-grpc-plugin /usr/lib/swift/linux/libswiftDispatch.so /usr/lib/swift/linux/libswiftDispatch.so
COPY --from=swift-grpc-plugin /usr/lib/swift/linux/libswiftGlibc.so /usr/lib/swift/linux/libswiftGlibc.so
COPY --from=swift-grpc-plugin /usr/lib/x86_64-linux-gnu/libatomic.so.1 /usr/lib/x86_64-linux-gnu/libatomic.so.1
COPY --from=swift-grpc-plugin /usr/lib/x86_64-linux-gnu/libstdc++.so.6 /usr/lib/x86_64-linux-gnu/libstdc++.so.6
COPY --from=swift-grpc-plugin /lib64/ld-linux-x86-64.so.2 /lib64/ld-linux-x86-64.so.2

COPY --from=kotlin-grpc-plugin /usr/local/bin/protoc-gen-grpckt /usr/local/bin/
COPY --from=kotlin-plugin /usr/local/bin/protoc-gen-kotlin /usr/local/bin/
COPY --from=kotlin-plugin /protoc-gen-kotlin/lib/. /usr/local/lib/

COPY --from=js-plugin /usr/local/bin/protoc-gen-grpc-web /usr/local/bin/

COPY --from=go-plugin /usr/local/bin/protoc-gen-go /usr/local/bin/
COPY --from=go-plugin /usr/local/bin/protoc-gen-grpc-gateway /usr/local/bin/
COPY --from=go-plugin /usr/local/bin/protoc-gen-go-grpc /usr/local/bin/

COPY --from=swagger-plugin /usr/local/bin/protoc-gen-swagger /usr/local/bin/

WORKDIR /home

ENTRYPOINT [ "prototool" ]